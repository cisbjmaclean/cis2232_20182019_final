package info.hccis.camper.rest;

import com.google.gson.Gson;
import info.hccis.camper.data.springdatajpa.CamperRepository;
import info.hccis.camper.entity.Camper;
import java.io.IOException;

import java.util.ArrayList;
import java.util.Iterator;
import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

@Path("/CamperService")
public class CamperService {

    @Resource
    private final CamperRepository cr;

    /**
     * Note that dependency injection (constructor) is used here to provide the
     * CamperRepository object for use in this class.
     *
     * @param servletContext Context
     * @since 20181109
     * @author BJM
     */
    public CamperService(@Context ServletContext servletContext) {
        ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
        this.cr = applicationContext.getBean(CamperRepository.class);
    }

    /**
     * This rest service will provide all users from the associated database.
     *
     * @return json string containing all user information.
     * @since 20181109
     * @author BJM
     */
    @GET
    @Path("/campers/{name}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUsers(@PathParam("name") String firstName) {

        ArrayList<Camper> theCampers = (ArrayList<Camper>) cr.findByFirstName(firstName);
        
        Gson gson = new Gson();
        int statusCode = 200;

        if (theCampers.isEmpty()) {
            statusCode = 204;
        }

        String temp = "";
        temp = gson.toJson(theCampers);

        return Response.status(statusCode)
                .entity(temp).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();

    }


    /**
     * This rest service will provide all users from the associated database.
     *
     * @return json string containing all user information.
     * @since 20181109
     * @author BJM
     */
    @GET
    @Path("/campers")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUsersAll() {

        ArrayList<Camper> theCampers = (ArrayList<Camper>) cr.findAll();
        
        
        
        
        Gson gson = new Gson();
        int statusCode = 200;

        if (theCampers.isEmpty()) {
            statusCode = 204;
        }

        String temp = "";
        temp = gson.toJson(theCampers);

        return Response.status(statusCode)
                .entity(temp).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();

    }    
    
    /**
     * POST operation which will update a camper.
     * @param jsonIn 
     * @return response including the json representing the new camper.
     * @throws IOException 
     */
    
    @POST
    @Path("/campers")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateCamper(String jsonIn) throws IOException {

//        ObjectMapper mapper = new ObjectMapper();
        
        //JSON from String to Object
        Gson gson = new Gson();
        Camper camper = gson.fromJson(jsonIn, Camper.class);
        //Camper camper = mapper.readValue(jsonIn, Camper.class);
        camper = cr.save(camper);
        String temp = "";
            temp = gson.toJson(camper);
        
        return Response.status(201).entity(temp).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();

    }
    

    
    
    
    
}
