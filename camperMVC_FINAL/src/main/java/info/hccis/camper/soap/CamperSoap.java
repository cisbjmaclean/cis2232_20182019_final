package info.hccis.camper.soap;

import info.hccis.camper.dao.CamperDAO;
import info.hccis.camper.entity.Camper;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 * Our class CamperSoap web service
 * @author bjmaclean
 * @since 20181108
 */
@WebService(serviceName = "CamperSoap")
public class CamperSoap {

 /**
 * Method to get the camper
 * @param id
 * @return Camper
 */
    @WebMethod(operationName = "getCamper")
    public Camper getCamper(@WebParam(name = "camperId") int camperId) {
     
        return CamperDAO.select(camperId);
        
    }
}
