package info.hccis.camper.bo;

import info.hccis.util.UtilityRest;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * This class will provide functionality related to the weather.
 *
 * @author bjmaclean
 * @since 20181118
 */
public class WeatherBO {

    private static final String URL = "http://apidev.accuweather.com/currentconditions/v1/1327.json?language=en&apikey=hoArfRosT1215";

    public static String getWeatherDescriptionMessage() {

        JSONArray jsonArray = new JSONArray(UtilityRest.getJsonFromRest(URL));
        JSONObject jsonObject1 = jsonArray.getJSONObject(0);
        JSONObject temperature = jsonObject1.getJSONObject("Temperature");
        JSONObject metric = temperature.getJSONObject("Metric");
        float currentTemperature = metric.getFloat("Value");

        String output = "("+currentTemperature+"C) ";
        if(currentTemperature < 0){
            output += "Bring your coat";
        }else if(currentTemperature > 20){
            output += "Bring your shorts";
        }else{
            output += "It is a normal day";
        }
        
        return output;
    }

}
