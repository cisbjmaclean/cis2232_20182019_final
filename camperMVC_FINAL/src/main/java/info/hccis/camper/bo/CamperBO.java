package info.hccis.camper.bo;

import info.hccis.camper.entity.Camper;

public class CamperBO {

    /**
     * Apply the dob validation that we want applied.  
     * 
     * @since 20181026
     * @author BJM
     * @param camper
     * @return String with an error
     */
    
    public static String validateDOB(Camper camper) {

        String error = "";
        String yearFromDOB = camper.getDob().substring(0, 4);
        try {
            int year = Integer.parseInt(yearFromDOB);
            if (year < 2002) {
                error = "Must be born after 2001";
            }
        } catch (Exception e) {
            error
                    = "Invalid dob";
        }

        return error;
        
        
    }

}
