package info.hccis.camper.web;

import info.hccis.camper.bo.WeatherBO;
import info.hccis.camper.dao.CamperDAO;
import info.hccis.camper.data.springdatajpa.CamperRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class OtherController {

     private final CamperRepository cr;

    /**
     * This constructor will inject the UserRepository object into this
     * controller. It will allow this class' methods to use methods from this
     * Spring JPA repository object.
     *
     * @since 20180524
     * @author BJM
     * @param ur
     */
    @Autowired
    public OtherController(CamperRepository cr) {
        this.cr = cr;
    }
    
    @RequestMapping("/")
    public String showHome(Model model) {

        //Get the weather description and add it to the model
        model.addAttribute("weatherDescription", WeatherBO.getWeatherDescriptionMessage());
        
        //Get the campers from the database
        model.addAttribute("campers", cr.findAll());
        
        //This will send the user to the list page
        return "camper/list";
    }
    @RequestMapping("/other/help")
    public String showHelp() {

        //This will send the user to the help view.
        return "other/help";
    }
    @RequestMapping("/other/about")
    public String showAbout(Model model) {

        
        //Get the weather description and add it to the model
        model.addAttribute("weatherDescription", WeatherBO.getWeatherDescriptionMessage());
        
        //This will send the user to the help view.
        return "other/about";
    }

}
