package info.hccis.camper.web;

import info.hccis.camper.bo.CamperBO;
import info.hccis.camper.dao.CamperDAO;
import info.hccis.camper.data.springdatajpa.CamperRepository;
import info.hccis.camper.entity.Camper;
import info.hccis.util.Utility;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class CamperController {

    private final CamperRepository cr;

    /**
     * This constructor will inject the UserRepository object into this
     * controller. It will allow this class' methods to use methods from this
     * Spring JPA repository object.
     *
     * @since 20180524
     * @author BJM
     * @param ur
     */
    @Autowired
    public CamperController(CamperRepository cr) {
        this.cr = cr;
    }

    @RequestMapping("/camper/list")
    public String camperList(Model model) {

        //Get the campers from the database
        //model.addAttribute("campers", CamperDAO.selectAll());
        model.addAttribute("campers", cr.findAll());

        //This will send the user to the list page
        return "camper/list";
    }

    @RequestMapping("/camper/add")
    public String camperAdd(Model model) {

        //put a camper object in the model to be used to associate with the input tags of 
        //the form.
        Camper newCamper = new Camper();
        model.addAttribute("camper", newCamper);

        //This will send the user to the welcome.html page.
        return "camper/add";
    }

    @RequestMapping("/camper/update")
    public String camperUpdate(Model model, HttpServletRequest request) {

        //- This method will use the registration id which is passed as a request 
        //parameter.  
        String idToFind = request.getParameter("id");
        System.out.println("BJM id passed=" + idToFind);
        //- It will go to the database and load the camper details into a camper
        //  object and put that object in the model.  

        //Camper editCamper = CamperDAO.select(Integer.parseInt(idToFind));
        //*******************************************************************
        //Use Spring data JPA instead
        Camper editCamper = cr.findOne(Integer.parseInt(idToFind));

        model.addAttribute("camper", editCamper);
        return "/camper/add";

    }

    @RequestMapping("/camper/find")
    public String camperFind(Model model, HttpServletRequest request) {

        //- This method will use the registration id which is passed as a request 
        //parameter.  
        String idToFind = request.getParameter("id");
        System.out.println("BJM id passed=" + idToFind);

        Camper findCamper = cr.findOne(Integer.parseInt(idToFind));

        //Reload the campers list so it can be shown on the next view.
        model.addAttribute("campers", cr.findByFirstNameAndLastName(findCamper.getFirstName(), findCamper.getLastName()));
        return "/camper/list";

    }

    @RequestMapping("/camper/delete")
    public String camperDelete(Model model, HttpServletRequest request) {

        //- This method will use the registration id which is passed as a request 
        //parameter.  
        String idToFind = request.getParameter("id");
        System.out.println("BJM id passed=" + idToFind);

        cr.delete(Integer.parseInt(idToFind));

//        
//        try {
//            //- It will go to the database and load the camper details into a camper
//            //  object and put that object in the model.
//            CamperDAO.delete(Integer.parseInt(idToFind));
//        } catch (Exception ex) {
//            System.out.println("Could not delete");
//        }
        //Reload the campers list so it can be shown on the next view.
        model.addAttribute("campers", cr.findAll());
        return "/camper/list";

    }

    @RequestMapping("/camper/addSubmit")
    public String camperAddSubmit(Model model, @Valid @ModelAttribute("camper") Camper theCamperFromTheForm, BindingResult result) {

        if (Utility.TESTING) {
            System.out.println("BJM-Checking validation." + result.getErrorCount());
        }

        //You can do your own validation if you need to.
        String error = CamperBO.validateDOB(theCamperFromTheForm);

        if (result.hasErrors() || error.length() > 0) {
            System.out.println("Error in validation.");
            model.addAttribute("message", error);

            return "/camper/add";
        }

        //Call the dao method to put this guy in the database.
        try {
//            CamperDAO.update(theCamperFromTheForm);
            cr.save(theCamperFromTheForm);
        } catch (Exception e) {
            System.out.println("Could not save to the database");
        }

        System.out.println("BJM-Did we get here?");
        //Reload the campers list so it can be shown on the next view.
        
        
        long size = cr.count();
        model.addAttribute("message", "Camper updated.  There are now "+size+" campers");
        model.addAttribute("campers", cr.findAll());
        //This will send the user to the welcome.html page.
        return "camper/list";
    }

}
