package info.hccis.util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Utility {
    public static final boolean TESTING = true;
    
    
    /**
     * This method will use md5 to create a hash value for the pw.
     *
     * @since 20170516
     * @author BJM
     */
    public static String getHashPassword(String inPassword) {
        try {
            return getMD5Hash(inPassword);
        } catch (Exception ex) {
            Logger.getLogger(Utility.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "Should not see this!";
    }

    public static String getMD5Hash(String passwordIn) throws Exception {
        MessageDigest m = MessageDigest.getInstance("MD5");
        m.update(passwordIn.getBytes(), 0, passwordIn.length());
        return "" + new BigInteger(1, m.digest()).toString(16);
    }

    /**
     * This wil;l give current date/time in format provided
     *
     * @since 20170516
     * @param format
     * @return formatted now
     */
    public static String getNow(String format) {
        if (format.isEmpty()) {
            format = "yyyy-MM-dd HH:mm:ss";
        }
        DateFormat dateFormat = new SimpleDateFormat(format);
        Date date = new Date();
        String now = dateFormat.format(date);
        return now;

    }
}
